
# B2C2 CLI

```b2c2 --help
Usage: b2c2 [OPTIONS] COMMAND [ARGS]...

Options:
  --verbose INTEGER  Standard python logging LEVEL (int) eg: 10 for INFO
  --help             Show this message and exit.

Commands:
  balance      GET balance
  instruments  GET Instruments
  quote        Create QUOTE
```

```b2c2 quote --help
Usage: b2c2 quote [OPTIONS]

  Create QUOTE

Options:
  -i, --instrument TEXT  Instrument name  [required]
  -s, --side [sell|buy]  sell or buy  [required]
  -q, --quantity FLOAT   qunatity to buy or sell  [required]
  --help                 Show this message and exit.
```

After creating quote there is a possibility to creare an order

## Requirments
* Python 3.9

## Used linters & tools
* mypy
* flake8
* isort
* black

Everything is checked in CI pipeline (gitlab)

## How to install?
* Create virtualenv for python3.9
* `pip install -r requirments.txt`
* `pip install .`

## How to run cli?
Just run a command
`b2c2`



## How to tests & linters?
Everthing can be executed at once by 

`tox .`

All others can be executed by Commands
* `flake8 .`
* `mypy .`
* `black --check .`
* `isort --recursive --check-only --diff .`
* `pytest`

## Logs
Logs are stored to the file logs.log

Example output:
```2021-10-17 15:15:24.006 | INFO     | services.balance:get_balance:10 - Fetching balance
2021-10-17 15:15:24.264 | INFO     | engine.console:request_quote:82 - Created order from quote
2021-10-17 15:15:32.293 | INFO     | services.instruments:get_instruments:16 - Fetching instruments
2021-10-17 15:15:32.577 | INFO     | services.quote:get_quote:28 - Creating Quote | data: {'instrument': 'LTCJPY.SPOT', 'side': 'sell', 'quantity': 99999999999.0, 'client_rfq_id': '3d102a7d-df1e-42f7-b30d-46749581e1b8'}
2021-10-17 15:15:32.830 | ERROR    | services.base:_make_request:63 - Request: https://api.uat.b2c2.net/request_for_quote/, data: b'{"instrument": "LTCJPY.SPOT", "side": "sell", "quantity": 99999999999.0, "client_rfq_id": "3d102a7d-df1e-42f7-b30d-46749581e1b8"}', response: {'errors': [{'field': 'quantity', 'message': 'Ensure that there are no more than 8 digits before the decimal point.', 'code': 1100}]})
2021-10-17 15:15:32.831 | ERROR    | engine.console:inner_function:28 - 400 Client Error: Bad Request for url: https://api.uat.b2c2.net/request_for_quote/
2021-10-17 15:15:46.027 | INFO     | services.instruments:get_instruments:16 - Fetching instruments
2021-10-17 15:15:46.308 | INFO     | services.quote:get_quote:28 - Creating Quote | data: {'instrument': 'LTCJPY.SPOT', 'side': 'sell', 'quantity': 1000.0, 'client_rfq_id': '6462d3ce-dee4-49c4-b90e-e16d8880864d'}
2021-10-17 15:15:46.568 | ERROR    | services.base:_make_request:63 - Request: https://api.uat.b2c2.net/request_for_quote/, data: b'{"instrument": "LTCJPY.SPOT", "side": "sell", "quantity": 1000.0, "client_rfq_id": "6462d3ce-dee4-49c4-b90e-e16d8880864d"}', response: {'errors': [{'field': 'non_field_errors', 'message': 'Risk exposure is too high: only 500000 allowed', 'code': 1012}]})
2021-10-17 15:15:46.568 | ERROR    | engine.console:inner_function:28 - 400 Client Error: Bad Request for url: https://api.uat.b2c2.net/request_for_quote/
```
## Example of usage
Normal flow
![Alt Text](demo/normal.gif)

With verbose (debug)
![Alt Text](demo/debug.gif)

