import decimal
from dataclasses import dataclass
from typing import Callable

from loguru import logger

from entities.instrument import Instrument
from services.balance import BalanceService
from services.instruments import InstrumentsService
from services.order import OrderService
from services.quote import QuoteService
from utils.exceptions import InstrumentNotFound, UnknownSide


@dataclass
class ApplicationEngine:
    SIDE_SELL = "sell"
    SIDE_BUY = "buy"

    stdin_confirm: Callable
    stdout: Callable

    instrument_service: InstrumentsService = InstrumentsService()
    balance_service: BalanceService = BalanceService()
    order_service: OrderService = OrderService()
    quote_service: QuoteService = QuoteService()

    def _get_instrument(self, name: str) -> Instrument:
        instrument = self.instrument_service.get_instrument(name)
        if not instrument:
            logger.warning(
                f"Trying to fetch non-existing instrument {instrument}", instrument=name
            )
            raise InstrumentNotFound
        return instrument

    def has_sufficient_balance(
        self,
        instrument: Instrument,
        price: decimal.Decimal,
        quantity: decimal.Decimal,
        side: str,
    ) -> bool:
        balance = self.balance_service.get_balance()
        counter_currency = instrument.get_counter_currency()
        currency = instrument.get_currency()
        if side == self.SIDE_BUY:
            return self._validate_buy(
                balance, currency, counter_currency, price, quantity
            )
        elif side == self.SIDE_SELL:
            return self._validate_sell(balance, currency, quantity)
        else:
            raise UnknownSide

    def _validate_buy(
        self,
        balance: dict,
        currency: str,
        counter_currency: str,
        price: decimal.Decimal,
        quantity: decimal.Decimal,
    ) -> bool:
        total_price = price * quantity
        is_sufficient_balance = (
            decimal.Decimal(balance[counter_currency]) >= price * quantity
        )
        if not is_sufficient_balance:
            message = (
                f"Insufficient balance for buying of {currency}."
                f" Balance [{counter_currency}]: {balance[counter_currency]}, requested: {total_price} {counter_currency}"
            )

            self.stdout(message)
            logger.warning(message)
        return is_sufficient_balance

    def _validate_sell(
        self, balance: dict, currency: str, quantity: decimal.Decimal
    ) -> bool:
        is_sufficient_balance = decimal.Decimal(balance[currency]) >= quantity
        if not is_sufficient_balance:
            message = (
                f"Insufficient balance for selling of {currency}."
                f" Balance [{currency}]: {balance[currency]}, requested quantity: {quantity}"
            )
            self.stdout(message)
            logger.warning(message)
        return is_sufficient_balance
