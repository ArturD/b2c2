import decimal
import uuid
from typing import Any, Callable, List, Optional

import click
import requests
from loguru import logger

from engine.base import ApplicationEngine
from utils.exceptions import InstrumentNotFound, InsufficientBalance


def exception_handler(func: Callable) -> Callable:
    def inner_function(*args: Any, **kwargs: Any) -> None:
        try:
            func(*args, **kwargs)
        except InstrumentNotFound:
            click.echo("Requested instrument doesn't exists or is unavailable")
        except InsufficientBalance:
            click.echo("Insufficient balance, please top up your account")
        except requests.HTTPError as e:
            error_message = "\n".join(
                [
                    f"[{error['code']}] {error['message']}"
                    for error in e.response.json().get("errors", [])
                ]
            )
            logger.error(e)
            click.echo(
                f"Couldnt proceed: {error_message}",
            )
        except Exception as e:
            logger.error(e)
            click.echo(
                f"Unknown error: {e}",
            )

    return inner_function


class ApplicationEngineConsole(ApplicationEngine):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(  # type: ignore
            stdout=click.echo, stdin_confirm=click.confirm, *args, **kwargs
        )

    @exception_handler
    def display_instruments(self) -> None:
        instruments = self.instrument_service.get_instruments()

        text = "\n".join(
            [
                instrument.name
                for instrument in self.instrument_service.get_instruments()
            ]
        )
        self.stdout(f"Available instruments ({len(instruments)}): \n {text}")

    @exception_handler
    def display_balance(self, only: Optional[List] = None) -> None:
        balance = self.balance_service.get_balance()
        if only:
            balance = dict((k, v) for k, v in balance.items() if k in only)

        message = "\n".join([f"{k}:{v}" for k, v in balance.items()])
        self.stdout(f"Balance:\n{message}")

    @exception_handler
    def request_quote(
        self, instrument_name: str, side: str, quantity: decimal.Decimal
    ) -> None:
        instrument = self._get_instrument(instrument_name)
        quote = self.quote_service.get_quote(
            instrument, side, quantity, str(uuid.uuid4())
        )
        self.stdout(f"Quote:\n{quote}")
        self.stdin_confirm("Do you want to send order?", abort=True)
        logger.info("Creating order from quote | {quote}", quote=quote)
        self.create_order(
            str(quote.instrument.underlier), quote.side, quote.quantity, quote.price  # type: ignore
        )
        logger.info("Created order from quote")

    @exception_handler
    def create_order(
        self,
        instrument_name: str,
        side: str,
        quantity: decimal.Decimal,
        price: decimal.Decimal,
    ) -> None:
        instrument = self._get_instrument(instrument_name)
        if not self.has_sufficient_balance(instrument, price, quantity, side):
            return

        order = self.order_service.create_order(instrument, side, quantity, price)
        if order.is_rejected():
            self.stdout("!! Order has been rejected !!")
        self.stdout(order)
        self.display_balance(
            only=[instrument.get_currency(), instrument.get_counter_currency()]
        )
