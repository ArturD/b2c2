import os

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
MOCKS_PATH = os.path.join(ROOT_DIR, "../tests/mocks/")
LOGS_PATH = os.path.join(ROOT_DIR, "../logs.log")
AUTHORIZATION_TOKEN = "e13e627c49705f83cbe7b60389ac411b6f86fee7"

QUANTITY_PRECISION = 4
COUNTER_CURRENCY_PREVISION = 5
