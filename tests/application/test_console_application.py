import json

from engine.console import ApplicationEngineConsole
from settings.settings import MOCKS_PATH


class TestApplicationEngineConsole:  # noqa
    application = ApplicationEngineConsole()

    def _mock_get(self, requests_mock, url, file_name=None, status_code=200):
        file_name = file_name if file_name else self.mock_file_name
        requests_mock.get(
            url,
            json=json.load(open(f"{MOCKS_PATH}/{file_name}")),
            status_code=status_code,
        )

    def _mock_balance(self, requests_mock, file_name="balance.json"):
        self._mock_get(requests_mock, self.application.balance_service._url, file_name)

    def _mock_instruments(self, requests_mock, file_name="instruments.json"):
        self._mock_get(
            requests_mock, self.application.instrument_service._url, file_name
        )

    def test_init_instance(self):
        assert isinstance(self.application, ApplicationEngineConsole)

    def test_application_execute_balance_display(self, requests_mock):
        self._mock_balance(requests_mock)
        application = ApplicationEngineConsole()
        application.display_balance()

    def test_application_execute_instruments_display(self, requests_mock):
        self._mock_instruments(requests_mock)
        application = ApplicationEngineConsole()
        application.display_instruments()
