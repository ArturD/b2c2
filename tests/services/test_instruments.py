import pytest

from serializers.instrument import Instrument
from services.instruments import InstrumentsService
from tests.services.base import BaseApiTest


class Test_InstrumentsService:  # noqa
    class Test_GET_calls(BaseApiTest):  # noqa
        service = InstrumentsService()
        mock_file_name = "instruments.json"

        def test_service_returns_list_of_instrument_entities(
            self, requests_mock
        ):  # noqa
            self._mock_get(requests_mock)
            instruments = self.service.get_instruments()
            assert all(
                [isinstance(instrument, Instrument) for instrument in instruments]
            )

        @pytest.mark.parametrize(
            "instrument_name, ",
            [
                "BTCUSD",
                "BTCUSD",
                "BTCEUR",
                "BTCGBP",
                "ETHBTC",
                "ETHUSD",
                "LTCUSD",
                "XRPUSD",
                "BCHUSD",
            ],
        )
        def test_get_instrument_returns_single_entity(
            self, instrument_name, requests_mock
        ):  # noqa
            self._mock_get(requests_mock)
            instrument = self.service.get_instrument(instrument_name)
            assert isinstance(instrument, Instrument)

        @pytest.mark.parametrize(
            "instrument_name, ",
            [
                "one",
                "tw",
                "sdd",
                "three",
            ],
        )
        def test_get_instrument_returns_none_for_non_existing_name(
            self, instrument_name, requests_mock
        ):  # noqa
            self._mock_get(requests_mock)
            instrument = self.service.get_instrument(instrument_name)
            assert instrument is None
