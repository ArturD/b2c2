import json

import pytest
import requests

from settings.settings import MOCKS_PATH


class BaseApiTest:
    service = None
    mock_file_name = None

    def _mock_get(self, requests_mock, file_name=None, status_code=200):
        file_name = file_name if file_name else self.mock_file_name
        requests_mock.get(
            self.service._url,
            json=json.load(open(f"{MOCKS_PATH}/{file_name}")),
            status_code=status_code,
        )

    def test_has_authorization_header_in_request(self, requests_mock) -> None:
        self._mock_get(requests_mock)
        response = self.service._get()
        assert "Authorization" in response.request.headers

    def test_http_response_is_200(self, requests_mock) -> None:
        self._mock_get(requests_mock)
        response = self.service._get()
        assert response.status_code == 200

    @pytest.mark.parametrize(
        "status_code", [400, 401, 404, 405, 406, 429, 500, 503, 1000, 1001, 1002, 1004]
    )
    def test_raise_http_exception_for_failure_status_code(
        self, status_code, requests_mock
    ):
        self._mock_get(requests_mock, status_code=status_code)
        with pytest.raises(requests.HTTPError):
            self.service._get()
