from services.balance import BalanceService
from tests.services.base import BaseApiTest


class Test_BalanceService:  # noqa
    class Test_GET_calls(BaseApiTest):  # noqa
        service = BalanceService()
        mock_file_name = "balance.json"

        def test_balance_call_returns_currency_dict(self, requests_mock):
            self._mock_get(requests_mock)
            balance = self.service.get_balance()
            assert type(balance) == dict
