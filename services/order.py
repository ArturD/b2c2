import datetime
import decimal
import time
import uuid

import marshmallow
from loguru import logger

from entities.instrument import Instrument
from entities.order import Order
from serializers.order import OrderSerializer
from services.base import BaseService


class OrderService(BaseService):
    _url = "https://api.uat.b2c2.net/order/"
    serializer = OrderSerializer()

    def create_order(
        self,
        instrument: Instrument,
        side: str,
        quantity: decimal.Decimal,
        price: decimal.Decimal,
    ) -> Order:
        payload = {
            "instrument": instrument.name,
            "side": side,
            "quantity": str(quantity),
            "client_order_id": str(uuid.uuid4()),
            "price": str(price),
            "order_type": "FOK",
            "valid_until": datetime.datetime.utcfromtimestamp(
                time.time() + 10
            ).strftime("%Y-%m-%dT%H:%M:%S"),
            "executing_unit": "risk-adding-strategy",
        }
        logger.info("Creating order | payload: {payload}", payload=payload)
        response = self._post(payload)
        order = self.serializer.load(response.json(), unknown=marshmallow.INCLUDE)
        status = "REJECTED" if order.is_rejected() else "ACCEPTED"
        logger.info(
            "Order created | {status} | order: {order}", order=order, status=status
        )
        return order
