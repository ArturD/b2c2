from typing import Any, no_type_check

import requests
from loguru import logger
from requests import HTTPError, Response

from settings.settings import AUTHORIZATION_TOKEN


class BaseService:
    _url: str

    def _get_authorization_headers(self) -> dict:
        return {"Authorization": f"Token {AUTHORIZATION_TOKEN}"}

    @no_type_check
    @staticmethod
    def raise_for_status(response: Response) -> None:
        http_error_msg = ""
        if isinstance(response.reason, bytes):
            try:
                reason = response.reason.decode("utf-8")
            except UnicodeDecodeError:
                reason = response.reason.decode("iso-8859-1")
        else:
            reason = response.reason

        if 400 <= response.status_code < 500:
            http_error_msg = "%s Client Error: %s for url: %s" % (
                response.status_code,
                reason,
                response.url,
            )

        elif 500 <= response.status_code < 600:
            http_error_msg = "%s Server Error: %s for url: %s" % (
                response.status_code,
                reason,
                response.url,
            )

        elif 1000 <= response.status_code:
            http_error_msg = "%s B2C2 Error: %s for url: %s" % (
                response.status_code,
                reason,
                response.url,
            )

        if http_error_msg:
            raise HTTPError(http_error_msg, response=response)

    def _make_request(
        self, method: str, *args: Any, **kwargs: Any
    ) -> requests.Response:
        response = requests.request(  # type: ignore
            method,
            self._url,
            headers=self._get_authorization_headers(),
            *args,
            **kwargs,
        )
        if 400 <= response.status_code:
            logger.error(
                f"Request: {self._url}, data: {str(response.request.body)}, response: {str(response.json())})"
            )
            self.raise_for_status(response)
        return response

    def _get(self) -> requests.Response:
        return self._make_request("GET")

    def _post(self, data: dict) -> requests.Response:
        return self._make_request("POST", json=data)
