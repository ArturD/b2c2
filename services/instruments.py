from typing import List, Optional

import marshmallow
from loguru import logger

from entities.instrument import Instrument
from serializers.instrument import InstrumentSerializer
from services.base import BaseService


class InstrumentsService(BaseService):
    _url = "https://api.uat.b2c2.net/instruments/"
    serializer = InstrumentSerializer(many=True)

    def get_instruments(self) -> List[Instrument]:
        logger.info("Fetching instruments")
        response = self._get()
        data = self.serializer.load(response.json(), unknown=marshmallow.INCLUDE)
        return data

    def get_instrument(self, name: str) -> Optional[Instrument]:
        instruments = self.get_instruments()
        instrument = next(
            iter(
                instrument for instrument in instruments if instrument.underlier == name
            ),
            None,
        )
        return instrument
