from loguru import logger

from services.base import BaseService


class BalanceService(BaseService):
    _url = "https://api.uat.b2c2.net/balance/"

    def get_balance(self) -> dict:
        logger.info("Fetching balance")
        response = self._get()
        return response.json()
