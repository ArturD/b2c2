import decimal

from loguru import logger

from entities.instrument import Instrument
from entities.quote import Quote
from serializers.quote import QuoteSerializer
from services.base import BaseService


class QuoteService(BaseService):
    _url = "https://api.uat.b2c2.net/request_for_quote/"
    serializer = QuoteSerializer()

    def get_quote(
        self,
        instrument: Instrument,
        side: str,
        quantity: decimal.Decimal,
        client_rfq_id: str,
    ) -> Quote:
        payload = {
            "instrument": instrument.name,
            "side": side,
            "quantity": quantity,
            "client_rfq_id": client_rfq_id,
        }
        logger.info("Creating Quote | data: {payload}", payload=payload)

        response = self._post(payload)
        data = self.serializer.load(response.json())
        return data
