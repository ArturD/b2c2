class ApplicationException(Exception):
    pass


class InstrumentNotFound(ApplicationException):
    pass


class InsufficientBalance(ApplicationException):
    pass


class UnknownSide(ApplicationException):
    pass
