import decimal
from dataclasses import dataclass
from datetime import datetime
from typing import Union

from entities.base import BaseEntity
from entities.instrument import Instrument


@dataclass
class Quote(BaseEntity):
    valid_until: datetime
    rfq_id: str
    client_rfq_id: str
    quantity: decimal.Decimal
    side: str
    instrument: Union[Instrument, str]
    price: decimal.Decimal
    created: datetime

    def __post_init__(self) -> None:
        self.instrument = (
            self.instrument
            if type(self.instrument) == Instrument
            else Instrument(name=str(self.instrument))
        )

    def __str__(self) -> str:
        return f"[{self.rfq_id}][{self.side}] Price: {self.price} {self.instrument} for {self.quantity} (Valid until: {self.valid_until})"
