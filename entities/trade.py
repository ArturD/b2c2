import decimal
from dataclasses import dataclass
from datetime import datetime
from typing import Optional, Union

from entities.base import BaseEntity
from entities.instrument import Instrument


@dataclass
class Trade(BaseEntity):
    instrument: Union[Instrument, str]
    trade_id: str
    origin: str
    rfq_id: Optional[str]
    created: datetime
    price: decimal.Decimal
    quantity: decimal.Decimal
    order: str
    side: str
    executing_unit: str
    cfd_contract: Optional[str] = None

    def __str__(self) -> str:
        return f"[{self.trade_id}][{self.side}] Price: {self.price} {self.instrument} for {self.quantity}"

    def __post_init__(self) -> None:
        if isinstance(self.instrument, str):
            self.instrument = Instrument(name=self.instrument)
