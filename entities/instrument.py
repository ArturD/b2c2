from dataclasses import dataclass
from typing import Optional

from entities.base import BaseEntity


@dataclass
class Instrument(BaseEntity):
    name: str
    underlier: Optional[str] = None
    type: Optional[str] = None

    def __post_init__(self) -> None:
        if not self.underlier:
            self.underlier = self.name.split(".")[0]
        if not self.type:
            name_splited = self.name.split(".")
            self.type = name_splited[1] if len(name_splited) > 1 else None

    def __str__(self) -> str:
        return self.name

    def get_counter_currency(self) -> str:
        instrument = self.name.split(".")[0]
        counter_currency = instrument[3:]
        return counter_currency

    def get_currency(self) -> str:
        instrument = self.name.split(".")[0]
        currency = instrument[:3]
        return currency
