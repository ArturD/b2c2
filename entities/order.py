import decimal
from dataclasses import dataclass
from datetime import datetime
from typing import List, Optional, Union

from entities.base import BaseEntity
from entities.instrument import Instrument
from entities.trade import Trade


@dataclass
class Order(BaseEntity):
    order_id: str
    client_order_id: str
    quantity: decimal.Decimal
    side: str
    instrument: Union[Instrument, str]
    price: str
    executed_price: Optional[decimal.Decimal]
    executing_unit: str
    trades: List[Trade]
    created: datetime
    order_type: str

    def __post_init__(self) -> None:
        if isinstance(self.instrument, str):
            self.instrument = Instrument(name=str(self.instrument))

    def __str__(self) -> str:
        trades = "\n".join([str(trade) for trade in self.trades])
        return (
            f"[{self.get_status().upper()}][{self.order_id}][{self.side}]\n"
            f" Price: {self.executed_price} {self.instrument} for {self.quantity}\n"
            f" Trades: {trades}"
        )

    def get_status(self) -> str:
        return "failure" if self.is_rejected() else "success"

    def is_rejected(self) -> bool:
        return not self.executed_price
