#!/usr/bin/env python
from setuptools import find_packages, setup

setup(
    name="B2C2",
    version="0.1.0",
    py_modules=["main"],
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        "Click",
    ],
    entry_points={
        "console_scripts": [
            "b2c2 = main:cli",
        ],
    },
)
