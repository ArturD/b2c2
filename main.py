import sys

import click
from loguru import logger

from engine.console import ApplicationEngineConsole
from settings.settings import LOGS_PATH

application = ApplicationEngineConsole()


@click.group()
@click.option(
    "--verbose",
    default=False,
    type=int,
    help="Standard python logging LEVEL (int) eg: 10 for INFO",
)
def cli(verbose: int) -> None:
    logger.remove()
    logger.add(LOGS_PATH, rotation="500 MB")
    logger.configure()
    if verbose:
        logger.add(sys.stderr, level=verbose)


@cli.command(name="instruments")
def command_get_instruments() -> None:
    """GET Instruments"""
    application.display_instruments()


@cli.command(name="balance")
def command_get_balance() -> None:
    """GET balance"""
    application.display_balance()


@cli.command(name="quote")
@click.option(
    "-i",
    "--instrument",
    "instrument_name",
    required=True,
    help="Instrument name",
    prompt="Instrument name",
)
@click.option(
    "-s",
    "--side",
    required=True,
    help="sell or buy",
    prompt="sell or buy?",
    type=click.Choice(["sell", "buy"], case_sensitive=False),
)
@click.option(
    "-q",
    "--quantity",
    required=True,
    help="quantity to buy or sell",
    prompt="Quantity",
    type=click.FLOAT,
)
def command_request_quote(instrument_name: str, side: str, quantity: float) -> None:
    """Create QUOTE"""
    application.request_quote(instrument_name, side, quantity)
