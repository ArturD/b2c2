import marshmallow_dataclass

from entities.instrument import Instrument

InstrumentSerializer = marshmallow_dataclass.class_schema(Instrument)
