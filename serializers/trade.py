import marshmallow_dataclass

from entities.trade import Trade

TradeSerializer = marshmallow_dataclass.class_schema(Trade)
