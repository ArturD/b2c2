import marshmallow_dataclass

from entities.quote import Quote

QuoteSerializer = marshmallow_dataclass.class_schema(Quote)
