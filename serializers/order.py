import marshmallow_dataclass

from entities.order import Order

OrderSerializer = marshmallow_dataclass.class_schema(Order)
